package com.yue.cloud.service.impl;

import com.yue.cloud.entity.Payment;
import com.yue.cloud.mapper.PaymentMapper;
import com.yue.cloud.service.IPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ：YueZheng
 * @description：TODO
 * @date ：2021/10/7 6:03 下午
 */
@Slf4j
@Service
public class PaymentServiceImpl implements IPaymentService {

    @Autowired
    private PaymentMapper paymentMapper;

    private static Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

    /**
     * 根据id查询payment
     *
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 7:07 下午
     * @Param: paymentId
     * @return payment
     */
    @Override
    public Payment getPaymentById(Long id) {
        Payment payment = paymentMapper.selectByPrimaryKey(id);
        logger.info("查询成功 。。。",  payment);
        return payment;
    }

    /**
     * 添加 payment
     *
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 7:08 下午
     * @Param: payment
     * @return int
     */
    @Override
    public int addPayment(Payment payment) {
        int insert = paymentMapper.insert(payment);
        if (insert > 0) {
            logger.info("数据保存成功 。。。");
        } else {
            logger.info("数据保存失败 。。。");
        }
        return insert;
    }
}
