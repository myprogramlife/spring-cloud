package com.yue.cloud.mapper;

import com.yue.cloud.entity.Payment;

/**
 * @Entity com.yue.cloud.entity.Payment
 */
public interface PaymentMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Payment record);

    int insertSelective(Payment record);

    Payment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Payment record);

    int updateByPrimaryKey(Payment record);

}
