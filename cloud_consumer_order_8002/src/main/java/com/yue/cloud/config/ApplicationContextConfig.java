package com.yue.cloud.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Spring注入自定义类
 *
 * @author ：YueZheng
 * @description：TODO
 * @date ：2021/10/7 10:59 下午
 */
@Configuration
public class ApplicationContextConfig {

    /**
     * 用于RPC远程调用
     *
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 11:01 下午
     * @Param: null
     * @return RestTemplate
     */
    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
