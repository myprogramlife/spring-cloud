package com.yue.cloud.service;

import com.yue.cloud.entity.Payment;

/**
 * @author ：YueZheng
 * @description：TODO
 * @date ：2021/10/7 6:02 下午
 */
public interface IOrderService {

    Payment getOrderById(Long id);

    int addOrder(Payment payment);

    Payment getPaymentById(Long id);

    int addPayment(Payment payment);
}
