package com.yue.cloud.service.impl;

import com.yue.cloud.entity.Payment;
import com.yue.cloud.mapper.OrderMapper;
import com.yue.cloud.service.IOrderService;
import com.yue.common.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author ：YueZheng
 * @description：TODO
 * @date ：2021/10/7 6:03 下午
 */
@Slf4j
@Service
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private RestTemplate restTemplate;

    private static Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    /**
     * 根据id查询payment
     *
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 7:07 下午
     * @Param: paymentId
     * @return payment
     */
    @Override
    public Payment getOrderById(Long id) {
        Payment payment = new Payment();
        logger.info("查询成功 。。。",  payment);
        return payment;
    }

    /**
     * 添加 payment
     *
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 7:08 下午
     * @Param: payment
     * @return int
     */
    @Override
    public int addOrder(Payment payment) {
        int insert = 0;
        if (insert > 0) {
            logger.info("数据保存成功 。。。");
        } else {
            logger.info("数据保存失败 。。。");
        }
        return insert;
    }

    /**
     * 根据id调用支付模块查询payment
     *
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 11:14 下午
     * @Param: id
     * @return payment
     */
    @Override
    public Payment getPaymentById(Long id) {
        String url = "http://127.0.0.1:8001/cloud/payment/";
        Result result = restTemplate.getForObject(url + id, Result.class);
        return null;
    }

    /**
     * 调用支付模块添加payment
     *
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 11:15 下午
     * @Param: payment
     * @return int
     */
    @Override
    public int addPayment(Payment payment) {
        String url = "http://127.0.0.1:8001/cloud/payment/add";
        restTemplate.postForObject(url,payment,Result.class);
        return 0;
    }
}
