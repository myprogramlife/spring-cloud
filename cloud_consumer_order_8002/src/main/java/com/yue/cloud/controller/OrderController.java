package com.yue.cloud.controller;

import com.yue.cloud.entity.Payment;
import com.yue.cloud.service.IOrderService;
import com.yue.common.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：YueZheng
 * @description：TODO
 * @date ：2021/10/7 6:02 下午
 */
@RestController
@RequestMapping("/cloud/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    /**
     * 根据Id查询
     *
     * @return payment
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 6:27 下午
     * @Param: paymentId
     */
    @GetMapping("/{id}")
    public Result getOrderById(@PathVariable("id") Long id) {
        Payment payment = orderService.getOrderById(id);
        return Result.success(payment);
    }

    /**
     * 添加payment
     *
     * @return null
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 6:27 下午
     * @Param: payment
     */
    @PostMapping("/add")
    public Result addOrder(@RequestBody Payment payment) {
        int i = orderService.addOrder(payment);
        if (i > 0) {
            return Result.success();
        } else {
            return Result.error();
        }
    }

    /**
     * 根据Id查询
     *
     * @return payment
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 6:27 下午
     * @Param: paymentId
     */
    @GetMapping("/payment/{id}")
    public Result getPaymentById(@PathVariable("id") Long id) {
        Payment payment = orderService.getPaymentById(id);
        return Result.success(payment);
    }

    /**
     * 添加payment
     *
     * @return null
     * @author: YueZheng
     * @description: TODO
     * @date: 2021/10/7 6:27 下午
     * @Param: payment
     */
    @PostMapping("/payment/add")
    public Result addPayment(@RequestBody Payment payment) {
        int i = orderService.addPayment(payment);
        if (i > 0) {
            return Result.success();
        } else {
            return Result.error();
        }
    }
}
